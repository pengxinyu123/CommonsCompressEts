/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ArArchiveInputStream, ArArchiveEntry, ArchiveStreamFactory, ArchiveOutputStream, File, OutputStream,
  InputStream, IOUtils, ArchiveEntry, Long } from '@ohos/commons-compress';
import featureAbility from '@ohos.ability.featureAbility';
import fileio from '@ohos.fileio';

@Entry
@Component
export struct ArTest {
  @State isCompressGArFileShow: boolean = false;
  @State isDeCompressGArShow: boolean = false;
  @State newFolder: string = 'newFolder';
  preTimestamp: number = 0;

  build() {
    Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center }) {
      Text('Ar相关功能')
        .fontSize(20)
        .margin({ top: 16 })

      Text('点击生成test1.xml')
        .fontSize(16)
        .margin({ top: 32 })
        .padding(8)
        .border({ width: 2, color: '#535353', radius: 6 })
        .onClick((event) => {
          if (!this.isFastClick()) {
            this.generateTextFile()
          }
        })

      if (this.isCompressGArFileShow) {
        Text('点击压缩test1.xml为ar文件')
          .fontSize(16)
          .margin({ top: 32 })
          .padding(8)
          .border({ width: 2, color: '#535353', radius: 6 })
          .onClick((event) => {
            if (!this.isFastClick()) {
              this.jsArTest()
            }
          })
      }

      if (this.isDeCompressGArShow) {
        Text('解析longfile_bsd.ar')
          .fontSize(16)
          .margin({ top: 32 })
          .padding(8)
          .border({ width: 2, color: '#535353', radius: 6 })
          .onClick((event) => {
            if (!this.isFastClick()) {
              this.jsUnArTest()
            }
          })
      }
    }
    .width('100%')
    .height('100%')
  }

  aboutToAppear() {
    var context = featureAbility.getContext();
    context.getFilesDir()
      .then((data) => {
        this.createFile(data + '/' + this.newFolder, '/longfile_bsd.ar', new Int8Array([
          33, 60, 97, 114, 99, 104, 62, 10, 35, 49, 47, 50, 56, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 49, 51, 49, 49, 50, 53,
          54, 53, 49, 49, 32, 32, 49, 48, 48, 48, 32, 32, 49, 48, 48, 48, 32, 32, 49, 48, 48, 54, 52, 52, 32, 32, 52, 50, 32, 32, 32,
          32, 32, 32, 32, 32, 96, 10, 116, 104, 105, 115, 95, 105, 115, 95, 97, 95, 108, 111, 110, 103, 95, 102, 105, 108, 101,
          95, 110, 97, 109, 101, 46, 116, 120, 116, 72, 101, 108, 108, 111, 44, 32, 119, 111, 114, 108, 100, 33, 10, 35, 49, 47,
          51, 54, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 49, 52, 53, 52, 54, 57, 52, 48, 49, 54, 32, 32, 49, 48, 48, 48, 32, 32,
          49, 48, 48, 48, 32, 32, 49, 48, 48, 54, 54, 52, 32, 32, 52, 48, 32, 32, 32, 32, 32, 32, 32, 32, 96, 10, 116, 104, 105, 115,
          95, 105, 115, 95, 97, 95, 108, 111, 110, 103, 95, 102, 105, 108, 101, 95, 110, 97, 109, 101, 95, 97, 115, 95, 119, 101,
          108, 108, 46, 116, 120, 116, 66, 121, 101, 10
        ]));
      })
      .catch((error) => {
        console.error('File to obtain the file directory. Cause: ' + error.message);
      })
  }

  createFile(data: string, fileName: string, arr: Int8Array | Int32Array): void {
    try {
      fileio.mkdirSync(data);
    } catch (err) {
    }
    const writer = fileio.openSync(data + fileName, 0o102, 0o666);
    fileio.writeSync(writer, arr.buffer);
    fileio.closeSync(writer);
  }

  generateTextFile(): void {
    var context = featureAbility.getContext();
    context.getFilesDir()
      .then((data) => {
        const writer = fileio.openSync(data + '/' + this.newFolder + '/test1.xml', 0o102, 0o666);
        fileio.writeSync(writer, "<?xml version = '1.0'?>\n"
        + "<!DOCTYPE connections>\n"
        + "<connections>\n"
        + "</connections>");
        fileio.closeSync(writer);
        AlertDialog.show({ title: '生成成功',
          message: '请查看手机路径' + data + '/' + this.newFolder + '/test1.xml',
          confirm: { value: 'OK', action: () => {
            this.isCompressGArFileShow = true
          } }
        })
      })
      .catch((error) => {
        console.error('File to obtain the file directory. Cause: ' + error.message);
      })
  }

  jsArTest(): void {
    var context = featureAbility.getContext();
    context.getFilesDir()
      .then((data) => {
        this.testArArchiveCreation(data)
      })
  }

  jsUnArTest(): void {
    var context = featureAbility.getContext();
    context.getFilesDir()
      .then((data) => {
        this.testReadLongNamesBSD(data)
      })
  }

  testArArchiveCreation(data: string) {
    let output: File = new File(data, this.newFolder + '.ar');
    let file1: File = new File(data + '/' + this.newFolder, 'test1.xml');

    let out: OutputStream = new OutputStream();
    let input1: InputStream = new InputStream();
    out.setFilePath(output.getPath());
    input1.setFilePath(file1.getPath());
    let os: ArchiveOutputStream = ArchiveStreamFactory.DEFAULT.createArchiveOutputStream("ar", out, "");
    os.putArchiveEntry(new ArArchiveEntry("test1.xml", Long.fromNumber(file1.length()), 0, 0,
      ArArchiveEntry.DEFAULT_MODE, Long.fromNumber(new Date().getTime() / 1000)));
    IOUtils.copy(input1, os);
    os.closeArchiveEntry();
    os.close();
    AlertDialog.show({ title: '压缩成功',
      message: '请查看手机路径 ' + data + '/',
      confirm: { value: 'OK', action: () => {
        this.isDeCompressGArShow = true
      } }
    })
  }

  isFastClick(): boolean {
    var timestamp = Date.parse(new Date().toString());
    if ((timestamp - this.preTimestamp) > 1500) {
      this.preTimestamp = timestamp;
      return false;
    } else {
      return true;
    }
  }

  testReadLongNamesBSD(data: string): void {
    let inFile: File = new File(data + '/' + this.newFolder, "longfile_bsd.ar");
    let input: InputStream = new InputStream();
    input.setFilePath(inFile.getPath());
    let s: ArArchiveInputStream = new ArArchiveInputStream(input);
    let tarArchiveEntry: ArchiveEntry = null;
    while ((tarArchiveEntry = s.getNextEntry()) != null) {
      let name: string = tarArchiveEntry.getName();
      let tarFile: File = new File(data + '/' + this.newFolder, name);
      if (name.indexOf('/') != -1) {
        try {
          let splitName: string = name.substring(0, name.lastIndexOf('/'));
          fileio.mkdirSync(data + '/' + this.newFolder + '/' + splitName);
        } catch (err) {
        }
      }
      let fos: OutputStream = null;
      try {
        fos = new OutputStream();
        fos.setFilePath(tarFile.getPath())
        let read: number = -1;
        let buffer: Int8Array = new Int8Array(1024);
        while ((read = s.readBytes(buffer)) != -1) {
          fos.writeBytesOffset(buffer, 0, read);
        }
      } catch (e) {
        throw e;
      } finally {
        fos.close();
      }
    }
    AlertDialog.show({ title: '解压成功',
      message: '请查看手机路径 ' + data + '/' + this.newFolder,
      confirm: { value: 'OK', action: () => {
      } }
    })
  }
}

let TAG = "ArTest-----"