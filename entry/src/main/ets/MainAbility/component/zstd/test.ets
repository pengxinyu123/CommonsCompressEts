/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import fileio from '@ohos.fileio';
import featureAbility from '@ohos.ability.featureAbility';
import { ZstdCompressor, IOUtils, InputStream, OutputStream, File, ZstdDecompressor, System
} from '@ohos/commons-compress';

@Entry
@Component
export struct ZSTDTest {
  @State isCompressBzip2FileShow: boolean = false;
  @State isDeCompressBzip2Show: boolean = false;
  preTimestamp: number = 0;
  compressedSize: number = 0;
  compressed: Int8Array;
  original: Int8Array;

  build() {
    Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center }) {
      Text('zstd相关功能')
        .fontSize(20)
        .margin({ top: 16 })

      Text('点击生成hello.txt')
        .fontSize(16)
        .margin({ top: 32 })
        .padding(8)
        .border({ width: 2, color: '#535353', radius: 6 })
        .onClick((event) => {
          if (!this.isFastClick()) {
            this.generateTextFile()
          }
        })

      if (this.isCompressBzip2FileShow) {
        Text('点击压缩hello.txt为zstd文件')
          .fontSize(16)
          .margin({ top: 32 })
          .padding(8)
          .border({ width: 2, color: '#535353', radius: 6 })
          .onClick((event) => {
            if (!this.isFastClick()) {
              this.testZstdCompressed()
            }
          })
      }


      if (this.isDeCompressBzip2Show) {
        Text('点击解压zstd文件')
          .fontSize(16)
          .margin({ top: 32 })
          .padding(8)
          .border({ width: 2, color: '#535353', radius: 6 })
          .onClick((event) => {
            if (!this.isFastClick()) {
              this.testZstdDecompressed()
            }
          })
      }
    }
    .width('100%')
    .height('100%')
  }

  generateTextFile(): void {
    var context = featureAbility.getContext();
    context.getFilesDir()
      .then((data) => {
        const writer = fileio.openSync(data + '/hello.txt', 0o102, 0o666);
        fileio.writeSync(writer, "hello, world!  adjasjdakjdakjdkjakjdakjskjasdkjaskjdajksdkjasdkjaksjdkja\n"
        + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
        + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
        + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
        + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
        + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
        + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
        + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
        + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
        + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
        + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
        + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
        + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs"
        );
        fileio.closeSync(writer);
        AlertDialog.show({ title: '生成成功',
          message: '请查看手机路径' + data + '/hello.txt',
          confirm: { value: 'OK', action: () => {
            this.isCompressBzip2FileShow = true
          } }
        })
      })
      .catch((error) => {
        console.error('File to obtain the file directory. Cause: ' + error.message);
      })
  }

  testZstdCompressed(): void {
    var context = featureAbility.getContext();
    context.getFilesDir()
      .then((data) => {
        console.info('directory obtained. Data:' + data);
        let time = System.currentTimeMillis()
        let compressor = new ZstdCompressor();

        let output = new File(data, "/hello.txt.zst");
        let input = new File(data, "/hello.txt");
        let input1 = new InputStream();
        let out = new OutputStream();

        input1.setFilePath(input.getPath());
        out.setFilePath(output.getPath());

        this.original = new Int8Array(input1._endPosition)
        IOUtils.readFully(input1, this.original);

        let maxCompressLength: number = compressor.maxCompressedLength(this.original.length);
        this.compressed = new Int8Array(maxCompressLength);
        this.compressedSize = compressor.getZstdFrameCompress(this.original, 0, this.original.length, this.compressed, 0, this.compressed.length);

        out.writeBytes(this.compressed);
        out.close()
        input1.close();
        let time1 = System.currentTimeMillis()
        console.log('consoleconsoleconsole: ' + (time1 - time));

        AlertDialog.show({ title: '压缩成功',
          message: '请查看手机路径 ' + data + '/hello.txt.zstd',
          confirm: { value: 'OK', action: () => {
            this.isDeCompressBzip2Show = true
          } }
        })
      })
      .catch((error) => {
        console.error('File to obtain the file directory. Cause: ' + error.message);
      })
  }

  testZstdDecompressed() {
    var context = featureAbility.getContext();
    context.getFilesDir()
      .then((data) => {
        console.info('directory obtained. Data:' + data);
        let time = System.currentTimeMillis()
        let compressor = new ZstdDecompressor();

        let output = new File(data, "/newhello.txt");
        let out = new OutputStream();
        out.setFilePath(output.getPath());

        let decompressed: Int8Array = new Int8Array(this.original.length);
        let compressedSize: number = compressor.decompress(this.compressed, 0, this.compressedSize, decompressed, 0, decompressed.length);

        out.writeBytes(decompressed)
        out.close()
        let time1 = System.currentTimeMillis()
        console.log('consoleconsoleconsole1: ' + (time1 - time));

        AlertDialog.show({ title: '解压成功',
          message: '请查看手机路径 ' + data + '/newhello.txt',
          confirm: { value: 'OK', action: () => {
            this.isDeCompressBzip2Show = true
          } }
        })
      })
      .catch((error) => {
        console.error('File to obtain the file directory. Cause: ' + error.message);
      })
  }

  isFastClick(): boolean {
    var timestamp = Date.parse(new Date().toString());
    if ((timestamp - this.preTimestamp) > 1500) {
      this.preTimestamp = timestamp;
      return false;
    } else {
      return true;
    }
  }
}


